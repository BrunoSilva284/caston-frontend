# Stage 1
FROM node:10-alpine as build-step
#FROM node:14.15.1-alpine3.10  as build-step
RUN mkdir -p /app
WORKDIR /app
COPY . /app
RUN npm install typescript@3.5.3
RUN npm install
RUN npm run build --prod
# Stage 2
FROM nginx:1.17.1-alpine
COPY --from=build-step /app/dist/cast-on /usr/share/nginx/html
COPY default.conf /etc/nginx/conf.d/default.conf

#docker build -t gabrielmuniz95/caston-artista-front:latest .
#docker run -d -it -p 80:80/tcp --name caston-artista-front --network infra-compose_my-network gabrielmuniz95/caston-artista-front:latest